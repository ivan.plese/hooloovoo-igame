package com.igame.codetest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertNull;
import static org.junit.Assert.assertEquals;

public class SubmissionAnagramTest {
    ScoreBoard scoreBoard;
    WordDictionaryHandler dictionary;
    AnagramWordMakingGame service;

    public SubmissionAnagramTest() {
        dictionary = new WordDictionaryHandlerImpl();
    }

    @Before
    public void setUp() throws Exception {
        scoreBoard = ScoreBoard.getInstance();
        service = new AnagramGameImpl("areallylongword", dictionary);
    }

    @After
    public void cleanUp() {
        scoreBoard.clear();
    }

    @Test
    public void testSubmission_validWords_returnScores() throws Exception {
        assertEquals(2, service.submitWord("1", "no"));
        assertEquals(4, service.submitWord("2", "grow"));
        assertEquals(0, service.submitWord("3", "bold"));
        assertEquals(0, service.submitWord("4", "glly"));
        assertEquals(6, service.submitWord("5", "woolly"));
        assertEquals(0, service.submitWord("6", "adder"));
    }

    @Test
    public void testSubmission_InvalidWord_return0() {
        assertEquals(0, service.submitWord("1", "fakeWordnotaword"));
    }

    @Test
    public void submitWord_multipleEntriesWithSameUser_eachEntryAddedSeparately() {
        service.submitWord("1", "no");
        service.submitWord("1", "bold");
        service.submitWord("1", "grow");
        service.submitWord("1", "woolly");
        assertEquals("1", service.getUserNameAtPosition(0));
        assertEquals("1", service.getUserNameAtPosition(1));
        assertEquals("1", service.getUserNameAtPosition(2));
        assertNull(service.getUserNameAtPosition(3));
    }

    @Test
    public void submitWord_multipleEntryWithSameScore_allAreAcceptedAndSortedBySubmissionTime() {
        service.submitWord("1", "woolly");
        service.submitWord("2", "woolly");
        service.submitWord("3", "woolly");
        assertEquals("1", service.getUserNameAtPosition(0));
        assertEquals("2", service.getUserNameAtPosition(1));
        assertEquals("3", service.getUserNameAtPosition(2));
    }

    @Test
    public void getScoreAtPosition_requestPositionNotOccupied_returnNull() {
        service.submitWord("1", "woolly");
        service.submitWord("2", "woolly");
        assertNull(service.getScoreAtPosition(3));
    }

    @Test
    public void getWordEntryAtPosition_requestPositionNotOccupied_returnNull() {
        service.submitWord("1", "woolly");
        service.submitWord("2", "woolly");
        assertNull(service.getWordEntryAtPosition(3));
    }

    @Test
    public void getUserNameAtPosition_requestPositionNotOccupied_returnNull() {
        service.submitWord("1", "woolly");
        service.submitWord("2", "woolly");
        assertNull(service.getUserNameAtPosition(3));
    }
}
