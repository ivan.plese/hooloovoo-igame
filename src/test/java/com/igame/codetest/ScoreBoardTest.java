package com.igame.codetest;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.SortedMap;

import static org.junit.Assert.assertEquals;

public class ScoreBoardTest {

    private static ScoreBoard scoreBoard;

    @BeforeClass
    public static void setUp() {
        scoreBoard = ScoreBoard.getInstance();
    }

    @Test
    public void getScores_populatedWithMoreThenTenItems_getMethodTruncatesAndSorts() {
        scoreBoard.addScore(new Score(1, LocalDateTime.now(), "1", "anyWord"));
        scoreBoard.addScore(new Score(1, LocalDateTime.now(), "1", "anyWord"));
        scoreBoard.addScore(new Score(1, LocalDateTime.now(), "1", "anyWord"));
        scoreBoard.addScore(new Score(1, LocalDateTime.now(), "1", "anyWord"));
        scoreBoard.addScore(new Score(1, LocalDateTime.now(), "1", "anyWord"));
        scoreBoard.addScore(new Score(1, LocalDateTime.now(), "1", "anyWord"));
        scoreBoard.addScore(new Score(1, LocalDateTime.now(), "1", "anyWord"));
        scoreBoard.addScore(new Score(1, LocalDateTime.now(), "1", "anyWord"));
        scoreBoard.addScore(new Score(1, LocalDateTime.now(), "1", "anyWord"));
        scoreBoard.addScore(new Score(1, LocalDateTime.now(), "1", "anyWord"));
        scoreBoard.addScore(new Score(1, LocalDateTime.now(), "1", "anyWord"));
        assertEquals(10, scoreBoard.getScores().size());
    }

    @Test
    public void clear_callAfterAddingScores_scoresAreEmpty() {
        scoreBoard.addScore(new Score(1, LocalDateTime.now(), "1", "anyWord"));
        scoreBoard.addScore(new Score(1, LocalDateTime.now(), "1", "anyWord"));
        scoreBoard.addScore(new Score(1, LocalDateTime.now(), "1", "anyWord"));
        scoreBoard.clear();
        assertEquals(0, scoreBoard.getScores().size());
    }
}