package com.igame.codetest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Vector;

public class WordDictionaryHandlerImpl implements WordDictionaryHandler {

    Vector v = new Vector();

    public WordDictionaryHandlerImpl() {
        try {
            URL url = new URL("https://www.scrabble.org.nz/assets/CSW15.txt");
            URLConnection dc = url.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(dc.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null)
                v.add(inputLine);
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean contains(String word) {
        return v.contains(word.toUpperCase());
    }

    @Override
    public int size() {
        return v.size();
    }
}
