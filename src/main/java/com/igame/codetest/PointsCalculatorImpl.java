package com.igame.codetest;

public class PointsCalculatorImpl implements PointsCalculator {

    @Override
    public int calculate(String word, char[] characterList) {
        char[] pool = copyArray(characterList);
        char[] wordChars = word.toCharArray();
        char copy[] = new char[wordChars.length];

        for (int i = 0; i < wordChars.length; i++) {
            char charOnIndex = wordChars[i];
            for (int j = 0;j<pool.length; j++) {
                if (charOnIndex == pool[j]) {
                    copy[i] = charOnIndex;
                    pool[j] = 0;
                    break;
                }
            }
        }
        for (int ii=0;ii<wordChars.length;ii++) {
            if (wordChars[ii] != copy[ii]) {
                return 0;
            }
        }
        return word.length();
    }

    private char[] copyArray(char[] input) {
        char copy[] = new char[input.length];
        for (int i = 0; i < input.length; i++) {
            copy[i] = input[i];
        }
        return copy;
    }
}
