package com.igame.codetest;

public interface PointsCalculator {
    int calculate(String word, char[] characterSet);
}
