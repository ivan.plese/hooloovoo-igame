package com.igame.codetest;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class Score {
    private int points;
    private LocalDateTime submitTime;
    private String userName;
    private String word;
}
