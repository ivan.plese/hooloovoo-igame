package com.igame.codetest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ScoreBoard {

    private final int MAX_INDEX = 10;
    private ReadWriteLock rwLock = new ReentrantReadWriteLock();
    private List<Score> scores = null;
    private static ScoreBoard instance;
    private ScoreBoard() {
        scores = new ArrayList<>();
    }

    public static ScoreBoard getInstance() {
        if (instance == null) {
            instance = new ScoreBoard();
        }
        return instance;
    }

    private void sortScoresAndKeepOnlyTopTen() {
        Lock writeLock = rwLock.writeLock();
        writeLock.lock();
        try {
            Collections.sort(scores, byPointsAndSubmitTime());
            int firstIndex = scores.size() < MAX_INDEX ? scores.size() : MAX_INDEX;
            scores.subList(firstIndex, scores.size()).clear();
        } finally {
            writeLock.unlock();
        }
    }

    public void addScore(Score score) {
        Lock writeLock = rwLock.writeLock();
        writeLock.lock();
        try {
            scores.add(score);
        } finally {
            writeLock.unlock();
        }
    }

    public List<Score> getScores() {
        sortScoresAndKeepOnlyTopTen();
        Lock readLock = rwLock.readLock();
        readLock.lock();
        try {
            return scores;
        } finally {
            readLock.unlock();
        }
    }

    private Comparator byPointsAndSubmitTime() {
        return Comparator.comparing(Score::getPoints).thenComparing(Score::getSubmitTime);
    }

    public void clear() {
        Lock writeLock = rwLock.writeLock();
        writeLock.lock();
        try {
            scores.clear();
        } finally {
            writeLock.unlock();
        }
    }
}
