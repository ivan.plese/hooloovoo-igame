package com.igame.codetest;

import java.time.LocalDateTime;

public class AnagramGameImpl implements AnagramWordMakingGame {

    private final int FIRST_INDEX = 0;
    private final int LAST_INDEX = 9;

    private char[] lettersPool;
    private WordDictionaryHandler dictionary;
    private PointsCalculator calculator;
    private ScoreBoard scores;

    public AnagramGameImpl(String areallylongword, WordDictionaryHandler dictionary) {
        this.scores = ScoreBoard.getInstance();
        this.calculator = new PointsCalculatorImpl();
        this.lettersPool = areallylongword
                .toLowerCase()
                .replaceAll("[^a-z]", "")
                .toCharArray();
        this.dictionary = dictionary;
    }

    @Override
    public int submitWord(String username, String word) {
        if (dictionary.contains(word)) {
            Score score = new Score(calculator.calculate(word, lettersPool), LocalDateTime.now(), username, word);
            if (score.getPoints() > 0) {
                scores.addScore(score);
            }
            return score.getPoints();
        }
        return 0;
    }

    @Override
    public String getUserNameAtPosition(int position) {
        if (positionIsValid(position)) {
            return scores.getScores().get(position).getUserName();
        }
        return null;
    }

    @Override
    public String getWordEntryAtPosition(int position) {
        if (positionIsValid(position)) {
            return scores.getScores().get(position).getWord();
        }
        return null;
    }

    @Override
    public Integer getScoreAtPosition(int position) {
        if (positionIsValid(position)) {
            return scores.getScores().get(position).getPoints();
        }
        return null;
    }

    private boolean positionIsValid(int position) {
        int lastIndex = scores.getScores().size() - 1;
        lastIndex = lastIndex < LAST_INDEX ? lastIndex : LAST_INDEX;
        return position >= FIRST_INDEX && position <= lastIndex;
    }
}
