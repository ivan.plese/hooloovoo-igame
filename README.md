### Anagram Word Making Game
#### Author Ivan Plese
#### Date: 8 March 2021

This project is an implementation of the "Anagram Word Making Game" exercise.

### Pre-request  

To run this application you need to have installed and setup Maven 3 and Java 11 

### How to run
From the root of the project run:
  
```mvn clean install```
